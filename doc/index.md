# Using GitLab's helm.gitlab.io

Documentation Organization:
- [Installation](installation/index.md)
- [Upgrading](installation/upgrade.md)
- [Advanced Configuration](advanced/index.md)
- [Charts](charts/index.md) (laid out as the charts are)
- [Example values.yaml files](../examples/index.md)
- [Minikube](minikube/index.md)
- [Helm](helm/index.md)
- [Troubleshooting](troubleshooting/index.md)
- [Weekly demos preparation](preparation/index.md)
